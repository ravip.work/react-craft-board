import React from "react";
import TemplateEditor from "./component/TemplateEditor";

const App = () => {
  return (
    <div className="">
      <div id="editor">
        <TemplateEditor />
      </div>
    </div>
  );
};

export default App;
