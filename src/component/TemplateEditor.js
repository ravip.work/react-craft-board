import React, { useState, useEffect } from "react";
import grapesjs from "grapesjs";
import gjsPresetWebpage from "grapesjs-preset-webpage";
import gjsForms from "grapesjs-plugin-forms";
import gjsBlocksBasic from "grapesjs-blocks-basic";
import pluginCountdown from "grapesjs-component-countdown";
import grapesjsTabs from "grapesjs-tabs";
import grapesjsCustomCode from "grapesjs-custom-code";
import thePlugin from "grapesjs-plugin-export";
import pluginStyle from "grapesjs-style-gradient";
import flexBoxPlugin from "grapesjs-blocks-flexbox";
import grapesjsPluginExport from "grapesjs-plugin-export";
import grapesjsPluginCkeditor from "grapesjs-plugin-ckeditor";
import grapesjsShapeDivider from "grapesjs-shape-divider";
import "../styles/main.scss";

const TemplateEditor = () => {
  const [editor, setEditor] = useState(null);

  useEffect(() => {
    const storageManagerOptions = {
      id: "gjs-",
      type: "local",
    };

    const presetWebpageOptions = {
      blocksBasic: false,
      blocks: [],
    };

    const thePluginOptions = {
      addExportBtn: true,
    };

    const pluginStyleOptions = {};

    const editor = grapesjs.init({
      container: "#editor",
      fromElement: true,
      pageManager: true,
      storageManager: storageManagerOptions,

      plugins: [
        gjsPresetWebpage,
        gjsForms,
        gjsBlocksBasic,
        pluginCountdown,
        grapesjsTabs,
        grapesjsCustomCode,
        thePlugin,
        pluginStyle,
        flexBoxPlugin,
        grapesjsPluginExport,
        grapesjsPluginCkeditor,
        grapesjsShapeDivider,
      ],
      pluginsOpts: {
        gjsPresetWebpage: presetWebpageOptions,
        gjsForms: {},
        thePlugin: thePluginOptions,
        pluginStyle: pluginStyleOptions,
        grapesjsPluginCkeditor: {},
        grapesjsShapeDivider: {},
      },
    });

    setEditor(editor);
  }, []);

  return (
    <div className="">
      <div id="editor"></div>
    </div>
  );
};

export default TemplateEditor;
